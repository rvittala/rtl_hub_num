CREATE OR REPLACE PROCEDURE mi_hub360_rtl_num_SP(integer,integer)
RETURNS integer EXECUTE AS CALLER
LANGUAGE NZPLSQL AS
BEGIN_PROC
 DECLARE 
 trnfm_last_yr_nbr ALIAS FOR $1; 
 trnfm_nbr ALIAS FOR $2; 
 START_DATE DATE;
 END_DATE DATE;
 PREV_START_DATE DATE;
 PREV_END_DATE DATE;
 
 
 BEGIN AUTOCOMMIT ON 
 
 RAISE NOTICE 'trnfm_last_yr_nbr: %',trnfm_last_yr_nbr; 
 RAISE NOTICE 'trnfm_nbr: %',trnfm_nbr;

 START_DATE =(SELECT D.start_date::DATE FROM NUM_RTL_HUB_DATE D WHERE (D.trnfm_last_yr_nbr= trnfm_last_yr_nbr) AND D.trnfm_nbr= trnfm_nbr)::DATE;
 END_DATE = (SELECT D.end_date::DATE FROM NUM_RTL_HUB_DATE D WHERE (D.trnfm_last_yr_nbr= trnfm_last_yr_nbr) AND D.trnfm_nbr= trnfm_nbr)::DATE; 
 
 PREV_START_DATE =(SELECT D.start_date::DATE FROM NUM_RTL_HUB_DATE D WHERE (D.trnfm_last_yr_nbr= 365) AND D.trnfm_nbr= trnfm_nbr)::DATE;
 PREV_END_DATE = (SELECT D.end_date::DATE FROM NUM_RTL_HUB_DATE D WHERE (D.trnfm_last_yr_nbr= 365) AND D.trnfm_nbr= trnfm_nbr)::DATE; 
 
 START_52_WEEK_CORE= select max(transaction_date)-366 from numerator_fact_v;
 END_52_WEEK_CORE= select max(transaction_date)-731 from numerator_fact_v;

 
 RAISE NOTICE 'START_DATE: %',START_DATE; 
 RAISE NOTICE 'END_DATE: %',END_DATE;
 RAISE NOTICE 'PREV_START_DATE: %',START_DATE; 
 RAISE NOTICE 'PREV_END_DATE: %',END_DATE;
 RAISE NOTICE 'START_52_WEEK_CORE: %',END_DATE;
 RAISE NOTICE 'END_52_WEEK_CORE: %',END_DATE;

 
 
-- This querry has Tab1 (Channel) and Tab 5 (sub_category).
-- Tab 1 chooses the competitor retailer groups. Tab 5 has all the numbers needed to make the tables by retailer department (level 5), category (level 3) and sub_category (level 2)

-- All output are based on retailer core (people who make up the top 80% of retailer sales) as defined using Numerator data
-- In choosing the competitor retailers, a filter limits to dept/cat/sub_cat combinations that have been mapped from Numerator to Retailer (mapping file also take care of unnecessary combinations in tab 5) 

-- This set up assumes the report is running on 9/30/19. The latest Numerator data for that day is feed_date= 9/26/19 (which has data up to 9/25/19)

-- here are 2 example reports:
-- Report 1 - Time Frame: Last Week, Compared to: Previous This Year
--    core: 05-01 to 09-25
--    cy: 2019-09-19 to 2019-09-25
--    py: 2018-09-20 to 2018-09-26

-- Report 2 - Time Frame: Last 4 Weeks (ending Saturday), Compared to: Previous This Year
--   cy: 2019-08-25 to 2019-09-21
--   py: 2018-08-26 to 2018-09-22


-- Report 3 - Time Frame: Last 13 Weeks (ending Saturday), Compared to: Previous This Year
--   cy: 2019-06-23 to 2019-09-21
--   py: 2018-06-24 to 2018-09-22

-- QUERY BELOW HAS THE REPORT 1 NUMBERS COMMENTED OUT AND REPORT 2 NUMBERS REVEALED BECAUSE IT WAS RUN ON REPORT 2 LAST

-- Tab 1 - By channel (also provides the competitors groups)
drop table PN1USSA1..NUM_RTL_HUB_CHN_SOW_00 if exists;
create table PN1USSA1..NUM_RTL_HUB_CHN_SOW_00  as 
select x.*, NPY, nvl(py_retailer_sector_spend, 0.0) as py_retailer_spend , nvl(py_percent_spend,0.0) as py_percent_spend,
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend as varchar(255))
     else 'undefined' end as pct_change_cy_vs_py
FROM 
        (select a.*, cy_retailer_spend / sector_spend as cy_percent_spend
        from 
                (select b.channel, x.banner_id, COUNT(*) AS NCY, sum(nvl(sum_amount,0.0)) as cy_retailer_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id 				
								join PN1USSA1..NUM_WF_MAPPING_FILE y
                                on (y.sector_description=i.sector_description and y.department_description=i.department_description and y.major_category_description=i.major_category_description 
								    and y.category_description=i.category_description and y.sub_category_description=i.sub_category_description) and wflineitem > 0 AND nlineitem > 0
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend                               from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > START_52_WEEK_CORE
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > START_52_WEEK_CORE
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE transaction_date between START_DATE AND END_DATE /* 2019-09-19 00:00:00' and '2019-09-25 00:00:00'   '2019-08-25 00:00:00' and '2019-09-21 00:00:00'  */
                group by b.channel, x.banner_id
                ) as a
        cross join
                (select sum(nvl(sum_amount,0.0)) as sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id 				
								join PN1USSA1..NUM_WF_MAPPING_FILE y
                                on (y.sector_description=i.sector_description and y.department_description=i.department_description and y.major_category_description=i.major_category_description 
								and y.category_description=i.category_description and y.sub_category_description=i.sub_category_description) and wflineitem > 0 AND nlineitem > 0
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > START_52_WEEK_CORE
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > START_52_WEEK_CORE
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date between START_DATE AND END_DATE /* '2019-09-19 00:00:00' and '2019-09-25 00:00:00'   '2019-08-25 00:00:00' and '2019-09-21 00:00:00'    */                    
                ) as b
        ) as x
LEFT JOIN  
        (select a.*, py_retailer_sector_spend / py_sector_spend as py_percent_spend
        from 
                (select b.channel, x.banner_id, COUNT(*) AS NPY, sum(nvl(sum_amount,0.0)) as py_retailer_sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id 				
								join PN1USSA1..NUM_WF_MAPPING_FILE y
                                on (y.sector_description=i.sector_description and y.department_description=i.department_description and y.major_category_description=i.major_category_description 
								and y.category_description=i.category_description and y.sub_category_description=i.sub_category_description) and wflineitem > 0 AND nlineitem > 0
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date <= START_52_WEEK_CORE                       
                                                AND transaction_date > END_52_WEEK_CORE                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date <= START_52_WEEK_CORE                       
                                        AND transaction_date > END_52_WEEK_CORE                      
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE  transaction_date between PREV_START_DATE AND PREV_END_DATE /* '2018-09-20 00:00:00' and '2018-09-26 00:00:00'    '2018-08-26 00:00:00' and '2018-09-22 00:00:00'    */              
                group by b.channel, x.banner_id
                ) as a
        cross join
                (select sum(nvl(sum_amount,0.0)) as py_sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id 				
								join PN1USSA1..NUM_WF_MAPPING_FILE y
                                on (y.sector_description=i.sector_description and y.department_description=i.department_description and y.major_category_description=i.major_category_description 
								and y.category_description=i.category_description and y.sub_category_description=i.sub_category_description) and wflineitem > 0 AND nlineitem > 0
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date <= START_52_WEEK_CORE                       
                                                AND transaction_date > END_52_WEEK_CORE                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date <= START_52_WEEK_CORE                       
                                        AND transaction_date > END_52_WEEK_CORE                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date between PREV_START_DATE AND PREV_END_DATE  /* '2018-09-20 00:00:00' and '2018-09-26 00:00:00'  '2018-08-26 00:00:00' and '2018-09-22 00:00:00'*/                     
                ) as b
        ) as y
                ON (x.channel = y.channel
                and x.banner_id = y.banner_id)
;
-- Success	9/30/2019 9:16 PM	9/30/2019 9:18 PM	144.5780000	188

-- 52 weeks Success	9/20/2019 11:10 PM	9/20/2019 11:13 PM	195.3620000	1847


-- TO GET TO THE TOP 4 MAKJOR COMPETITORS OF TEH RETAILER, the retialer and the rest (Others)
drop table RTL_HUB_CHN_EXCEPT_RETAILER if exists;
create temporary table RTL_HUB_CHN_EXCEPT_RETAILER as
SELECT channel,banner_id,ncy,cy_retailer_spend,cy_percent_spend,npy,py_retailer_spend,py_percent_spend FROM 
(SELECT *, ROW_NUMBER() OVER (ORDER BY cy_percent_spend DESC) AS rank FROM PN1USSA1..NUM_RTL_HUB_CHN_SOW_00 where banner_id not in ('shop_rite')) tmp 
WHERE rank <= 4
;
drop table RTL_HUB_CHN_OTHERS if exists;
create temporary table RTL_HUB_CHN_OTHERS as 
SELECT 'MIXED' as channel, 'OTHERS' as banner_id,
sum(ncy) as ncy, sum(cy_retailer_spend) as cy_retailer_spend, sum(cy_percent_spend) as cy_percent_spend,  /* cannot add percents for OTHERSin general, but here it will work because of common divisor. it is redeifend later ANYWAY */
sum(npy) as npy, sum(py_retailer_spend) as py_retailer_spend, sum(py_percent_spend) as py_percent_spend
FROM PN1USSA1..NUM_RTL_HUB_CHN_SOW_00 a
left join (select distinct channel, banner_id from RTL_HUB_CHN_EXCEPT_RETAILER) b on (a.channel=b.channel and a.banner_id=b.banner_id)
where a.banner_id not in ('shop_rite') and b.banner_id is null
;
-- FOLLOWING TABLE IS NEEDED FOR ALL TABS
drop table PN1USSA1..RTL_HUB_BANNERS6  if exists;
create table PN1USSA1..RTL_HUB_BANNERS6 as
select * from RTL_HUB_CHN_EXCEPT_RETAILER a
union all 
select * from RTL_HUB_CHN_OTHERS b
union all
select channel, banner_id, ncy, cy_retailer_spend, cy_percent_spend, npy, py_retailer_spend, py_percent_spend from PN1USSA1..NUM_RTL_HUB_CHN_SOW_00 where banner_id in ('shop_rite')
;
-- Success	9/20/2019 11:15 PM	9/20/2019 11:15 PM	1.2680000	6

-- Data for Visual 1 (This is not needed as these numbers will be calculated in the Pivot or Tablau)
--drop table PN1USSA1..NUM_RTL_HUB_CHN_SOW_02 if exists;
Insert into  PN1USSA1..NUM_RTL_HUB_CHN_SOW_02 
select channel,	banner_id, ncy,	cy_retailer_spend, cy_percent_spend, npy, py_retailer_spend, py_percent_spend, 
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend as varchar(255)) 
else 'undefined' end as pct_change_cy_vs_py,
ROW_NUMBER() OVER (ORDER BY cy_percent_spend DESC) AS rank,
'trnfm_nbr','trnfm_last_yr_nbr'

from PN1USSA1..RTL_HUB_BANNERS6 order by cy_percent_spend desc;


-- Tab 5: sub-category (at retialer level 5, level 3 and level 2 - this output can generate all the other yables)

-- First create the table by Numerator fields:  sector, department, MAJOR CATEGORY category, SUB CATEGROY, channel and banner_id
-- Then convert the Numerator fields to retailer
-- Then limit to the competitor groups defined under tab1

drop table PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_00 if exists;
create table PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_00 as 
select x.*, NPY, nvl(py_retailer_subcategory_spend, 0.0) as py_retailer_subcategory_spend , nvl(py_percent_spend,0.0) as py_percent_spend
FROM 
        (select a.*, cy_retailer_subcategory_spend / NULLIF(subcategory_spend,0) as cy_percent_spend
        from 
                (select sector_description, department_description, major_category_description, category_description, sub_category_description, channel, x.banner_id, 
				        COUNT(*) AS NCY, sum(nvl(sum_amount,0.0)) as cy_retailer_subcategory_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / NULLIF(total_spend,0) as percent_spend                               from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > START_52_WEEK_CORE
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > START_52_WEEK_CORE
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE transaction_date between START_DATE AND END_DATE  /* 2019-09-19 00:00:00' and '2019-09-25 00:00:00'   '2019-08-25 00:00:00' and '2019-09-21 00:00:00'  */   
                group by sector_description, department_description, major_category_description, category_description, sub_category_description, channel, x.banner_id
                ) as a
        JOIN
                (select department_description, major_category_description, category_description, sub_category_description, sum(nvl(sum_amount,0.0)) as subcategory_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum /  NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > START_52_WEEK_CORE
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > START_52_WEEK_CORE
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date between START_DATE AND END_DATE /* 2019-09-19 00:00:00' and '2019-09-25 00:00:00'   '2019-08-25 00:00:00' and '2019-09-21 00:00:00'   */                    
                group by department_description, major_category_description, category_description, sub_category_description
                ) as b
                        ON a.department_description = b.department_description
						and a.major_category_description = b.major_category_description
                        and a.category_description = b.category_description
						and a.sub_category_description = b.sub_category_description
                        AND a.sub_category_description != ''
                        AND a.category_description != ''
						AND a.major_category_description != ''
						AND a.department_description != ''
        ) as x
LEFT JOIN  
        (select a.*, py_retailer_subcategory_spend /  NULLIF(py_subcategory_spend,0) as py_percent_spend
        from 
                (select department_description, major_category_description, category_description, sub_category_description, channel, x.banner_id, 
				        COUNT(*) AS NPY, sum(nvl(sum_amount,0.0)) as py_retailer_subcategory_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum /  NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND  transaction_date <= START_52_WEEK_CORE                       
                                                AND transaction_date > END_52_WEEK_CORE                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND  transaction_date <= START_52_WEEK_CORE                       
                                        AND transaction_date > END_52_WEEK_CORE                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE  transaction_date between PREV_START_DATE AND PREV_END_DATE  /* '2018-09-20 00:00:00' and '2018-09-26 00:00:00'    '2018-08-26 00:00:00' and '2018-09-22 00:00:00' */                
                group by department_description, major_category_description, category_description, sub_category_description, channel, x.banner_id
                ) as a
        JOIN
                (select department_description, major_category_description, category_description, sub_category_description, sum(nvl(sum_amount,0.0)) as py_subcategory_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum /  NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND  transaction_date <= START_52_WEEK_CORE                       
                                                AND transaction_date > END_52_WEEK_CORE                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date <= START_52_WEEK_CORE                       
                                        AND transaction_date > END_52_WEEK_CORE                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date between  PREV_START_DATE AND PREV_END_DATE  /* '2018-09-20 00:00:00' and '2018-09-26 00:00:00'    '2018-08-26 00:00:00' and '2018-09-22 00:00:00'   */                    
                group by department_description, major_category_description, category_description, sub_category_description
                ) as b
                        ON  a.department_description = b.department_description
						and a.major_category_description = b.major_category_description
                        and a.category_description = b.category_description
						and a.sub_category_description = b.sub_category_description
                        AND a.sub_category_description != ''
                        AND a.category_description != ''
						AND a.major_category_description != ''
						AND a.department_description != ''
        ) as y
                ON x.department_description = y.department_description
				AND x.major_category_description = y.major_category_description
                AND x.category_description = y.category_description
				AND x.sub_category_description = y.sub_category_description
                AND x.channel = y.channel
                and x.banner_id = y.banner_id
WHERE x.department_description != 'unknown'                
order by x.department_description, x.major_category_description, x.category_description, x.sub_category_description
;
-- Success	9/30/2019 11:10 PM	9/30/2019 11:10 PM	40.4930000	9393
-- 52 week Success	9/20/2019 11:18 PM	9/20/2019 11:22 PM	202.8470000	111085

-- Reduce to banners6 chosen at Overall spend
drop table NUM_RTL_HUB_SUBCAT_SOW_OTHERS if exists ;
create temporary table NUM_RTL_HUB_SUBCAT_SOW_OTHERS as
select sector_description, department_description, major_category_description, category_description, sub_category_description,
'MIXED' as channel, 'OTHERS' as banner_id,
sum(ncy) as ncy, sum(cy_retailer_subcategory_spend) as cy_retailer_subcategory_spend, sum(cy_percent_spend) as cy_percent_spend,  
sum(npy) as npy, sum(py_retailer_subcategory_spend) as py_retailer_subcategory_spend, sum(py_percent_spend) as py_percent_spend
from 
PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_00 a
left join (select distinct banner_id from PN1USSA1..RTL_HUB_BANNERS6 where banner_id <> 'OTHERS') b on a.banner_id=b.banner_id
where b.banner_id is null
group by 1,2,3,4,5,6,7;
-- Success	9/20/2019 11:23 PM	9/20/2019 11:23 PM	2.0460000	3542
drop table NUM_RTL_HUB_SUBCAT_SOW_04 if exists;
create temporary table NUM_RTL_HUB_SUBCAT_SOW_04 as
select a.*
from 
PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_00 a
left join (select distinct banner_id from PN1USSA1..RTL_HUB_BANNERS6 where banner_id <> 'OTHERS') b on a.banner_id=b.banner_id
where b.banner_id is not null;
-- Success	9/20/2019 11:23 PM	9/20/2019 11:26 PM	165.2910000	10059
drop table NUM_RTL_HUB_SUBCAT_SOW_01 if exists; 
create temporary table NUM_RTL_HUB_SUBCAT_SOW_01 as
select * from NUM_RTL_HUB_SUBCAT_SOW_04 a
union all 
select * from NUM_RTL_HUB_SUBCAT_SOW_OTHERS;
-- Success	9/20/2019 11:26 PM	9/20/2019 11:27 PM	29.9930000	13601
drop table PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_02 if exists;
create table PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_02 as 
SELECT sector_description, department_description, major_category_description, category_description, sub_category_description, channel, banner_id, 
ncy, cy_retailer_subcategory_spend, cy_percent_spend, npy, py_retailer_subcategory_spend,  py_percent_spend, 
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend AS VARCHAR(255))
else 'undefined' end as pct_change_cy_vs_py,
ROW_NUMBER() OVER (PARTITION BY department_description, major_category_description, category_description, sub_category_description ORDER BY cy_retailer_subcategory_spend DESC) AS rank FROM 
(
select a.* from 
NUM_RTL_HUB_SUBCAT_SOW_01 a
join
(select distinct department_description, major_category_description, category_description, sub_category_description from NUM_RTL_HUB_SUBCAT_SOW_01 where banner_id='shop_rite' and cy_percent_spend > 0.00) b 
on (a.department_description=b.department_description and a.major_category_description=b.major_category_description and a.category_description=b.category_description and a.sub_category_description=b.sub_category_description)
) c
order by sector_description, department_description, major_category_description, category_description, sub_category_description, rank;
-- Success	9/20/2019 11:27 PM	9/20/2019 11:28 PM	13.4810000	10426

-- Crosssing to Retailer level2(subcategory) level3(category) and level5(department) descriptions
drop table ks_tab5_rtlr_fields if exists;
create temp table ks_tab5_rtlr_fields as
select trade_item_hier_l5_cd,max(trade_item_hier_l5_desc) as trade_item_hier_l5_desc, trade_item_hier_l3_cd,max(trade_item_hier_l3_desc) as trade_item_hier_l3_desc, trade_item_hier_l2_cd,max(trade_item_hier_l2_desc) as trade_item_hier_l2_desc, c.banner_id,   /* we can add channel here but it is a Numerator field. Also it is not used in the SOW report */
sum(ncy) as ncy, sum(cy_retailer_subcategory_spend) as cy_retailer_subcategory_spend,    /* cannot add percents here - define it later */
sum(npy) as npy, sum(py_retailer_subcategory_spend) as py_retailer_subcategory_spend
from PN1USSA1..RTL_HUB_MAPPING_FILE a
join PN1USSA1..NUM_WF_MAPPING_FILE b on (a.wflineitem=b.wflineitem)
right join PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_02 c on (b.sector_description=c.sector_description and b.department_description=c.department_description and 
b.major_category_description=c.major_category_description and b.category_description=c.category_description and b.sub_category_description=c.sub_category_description)
group by 1,3,5,7
;
-- Success	9/20/2019 11:28 PM	9/20/2019 11:28 PM	4.6510000	3371
drop table ks_tab5_rtlr_fields_02 if exists;
create temp table ks_tab5_rtlr_fields_02 as
select a.*, cy_retailer_subcategory_spend / cy_rtlr_subcat_tot as cy_percent_spend, py_retailer_subcategory_spend / py_rtlr_subcat_tot as py_percent_spend
from ks_tab5_rtlr_fields a
left join
(select trade_item_hier_l5_desc, trade_item_hier_l3_desc, trade_item_hier_l2_desc, sum(nvl(cy_retailer_subcategory_spend,0.0)) as cy_rtlr_subcat_tot, sum(nvl(py_retailer_subcategory_spend,0.0)) as py_rtlr_subcat_tot
from ks_tab5_rtlr_fields group by 1,2,3 having py_rtlr_subcat_tot>0) b
on (a.trade_item_hier_l5_desc=b.trade_item_hier_l5_desc and a.trade_item_hier_l3_desc=b.trade_item_hier_l3_desc and a.trade_item_hier_l2_desc=b.trade_item_hier_l2_desc);
-- Success	9/20/2019 11:29 PM	9/20/2019 11:29 PM	13.9580000	3371

-- Define percent_spent and change within each cell in case needed (but these most likely are calculated in Pivot or Tablau)
--drop table PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_03 if exists;
insert into PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_03 
select *,
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend AS VARCHAR(255))
else 'undefined' end as pct_change_cy_vs_py,
ROW_NUMBER() OVER (PARTITION BY trade_item_hier_l5_desc, trade_item_hier_l3_desc, trade_item_hier_l2_desc ORDER BY cy_retailer_subcategory_spend DESC) AS rank,
'trnfm_nbr' AS trnfm_nbr ,'trnfm_last_yr_nbr' AS trnfm_last_yr_nbr,'banner_nm' AS banner_nm
from ks_tab5_rtlr_fields_02;
-- Success	9/20/2019 11:30 PM	9/20/2019 11:31 PM	56.1180000	3371

/*select * from PN1USSA1..NUM_RTL_HUB_CHN_SOW_02;
select * from PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_03 order by trade_item_hier_l5_desc, trade_item_hier_l3_desc, trade_item_hier_l2_desc, rank;

-- Add Sales
select banner_nm, trnfm_nbr, product, count(*) from PN1USSA1..HUB_RTL_CE_SUBCAT_FACT_02 group by 1,2,3; 
select count(*) from PN1USSA1..HUB_RTL_CE_SUBCAT_FACT_02;
233362
*/

-- Need to output category combinations that are not in Scott's mapping table.  Mail this to Scott so he can add them the next time.
--drop table ks_new_cat_combinations_to_scott if exists;
--one time run no need to run for all aggreagations

/*drop table ks_new_cat_combinations_to_scott if exists;
create table ks_new_cat_combinations_to_scott as
select a.wflineitem, b.nlineitem, c.channel, c.banner_id,   
c.sector_description, c.department_description, c.major_category_description, c.category_description, c.sub_category_description,
ncy, cy_retailer_subcategory_spend, npy, py_retailer_subcategory_spend
from PN1USSA1..RTL_HUB_MAPPING_FILE a
join PN1USSA1..NUM_WF_MAPPING_FILE b on (a.wflineitem=b.wflineitem)
full join PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_02 c on (b.sector_description=c.sector_description and b.department_description=c.department_description and 
b.major_category_description=c.major_category_description and b.category_description=c.category_description and b.sub_category_description=c.sub_category_description)
where c.banner_id in ('shop_rite','price_rite') and
(a.wflineitem is null or b.wflineitem is null or nlineitem is null); */
-- 688 records


-- NOTES: The following is needed but not done here as retailer sales should match with other reports in HUB
-- Need to add the retailer sales to PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_03 table so that the ordering needed for the visuals can be made.
-- For the 2nd visual "by department (level 5)" Scott need to answer how they need to be sorted (whose sales? Numerator or Retailer?)
-- For heat map, the size of the rectangle come from retilaer sales
-- weed out small cells - is this done at Tablau?


-- The final table PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_03 can be used to get all other tables needed for the SOW report (see th eExcel output)
-- All other tables in this query can be made temp tables once the whole querry is going to run as a single job.
-- This code has gone through many tweaks, there are a number of places that can be re-written to improve the run time


/*select * from NUM_RTL_HUB_SUBCAT_SOW_04
join ks_shoprite_sales on(trade_item_hier_l5_cd=trade_item_hier_l5_cd)
*/




-- retialer sales by department, category and sub-category
--drop table ks_shoprite_sales if exists;
insert into table ks_shoprite_sales as
select trade_item_hier_l5_cd,max(trade_item_hier_l5_desc) as trade_item_hier_l5_desc, trade_item_hier_l3_cd,max(trade_item_hier_l3_desc) as trade_item_hier_l3_desc, trade_item_hier_l2_cd,max(trade_item_hier_l2_desc) as trade_item_hier_l2_desc, 
sum(purch_amt) as sales,
'trnfm_nbr',
'trnfm_last_yr_nbr',
'banner_nm'
from 
trade_item_v  
join trade_item_owner_hierarchy_v tr using (trade_item_key) 
join ord_trd_itm_fact_ne_v using (trade_item_key)  
join touchpoint_v on (ord_touchpoint_key = touchpoint_key)
where
ntwk_id = 66 and             /*   USE THIS TO SEPARATE SHOPRITE (66) FROM PRICERITE (301). Also, one can use: ntwk_nm='ShopRite'     */
 tr.lgl_entity_nbr = 38 and /* YOU CAN ALSO USE tr.owner_key = 52020377150 */
ord_date_key between to_char(date(START_DATE),'YYYYMMDD') AND to_char(date(END_DATE),'YYYYMMDD')   /* to_char(date('2019-09-19'),'YYYYMMDD') and to_char(date('2019-09-25'),'YYYYMMDD')   to_char(date('2019-08-25'),'YYYYMMDD') and to_char(date('2019-09-21'),'YYYYMMDD')*/ and 
trade_item_hier_l1_cd <> -1 and tr.lgl_entity_nbr <> 1000 and 
purch_qty > 0 and purch_amt > 0
group by 1,3,5;


 RETURN 0; 
 END; 
 END_PROC;