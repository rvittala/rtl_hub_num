CREATE OR REPLACE PROCEDURE mi_hub360_rtl_num_MAIN_SP(integer)
RETURNS integer EXECUTE AS CALLER
LANGUAGE NZPLSQL AS
BEGIN_PROC
 DECLARE 
 WEEK_ENDING DATE;
 DOW_NBR ALIAS FOR $1;
 
 BEGIN AUTOCOMMIT ON 
 
 WEEK_ENDING = (SELECT D.CAL_DT::DATE FROM DATE_LU_V D WHERE (D.CAL_DY_RANK_NBR BETWEEN 0 AND 6) AND D.CAL_DOW_NBR = DOW_NBR)::DATE; 
 
 RAISE NOTICE 'RUNNING FOR WEEK ENDING: %',WEEK_ENDING;
 RAISE NOTICE 'DOW_NBR Value: %',DOW_NBR;
 
 
 
 DROP TABLE NUM_RTL_HUB_DATE IF EXISTS ;
 CREATE TABLE NUM_RTL_HUB_DATE as 
				select trnfm_last_yr_nbr,TRNFM_NBR, 
			   to_char(TO_DATE(min(date_key),'YYYYMMDD')- interval '6 days','yyyy-mm-dd') AS START_DATE,
			   to_char(TO_DATE(max(date_key),'YYYYMMDD') ,'yyyy-mm-dd')  AS END_DATE 
				from
                DATE_CALENDAR_YEAR_WK2DY_RLU_V A16            
                JOIN      DATE_TRNFM_CALENDAR_YR_WK_V DT          ON (DT.TRNFM_CAL_YR_WK_STOP_DT = A16.CAL_YR_WK_STOP_DT)
                WHERE A16.CAL_DOW_NBR = DOW_NBR
                AND DT.CAL_DOW_NBR = DOW_NBR
                AND DT.TRNFM_LAST_YR_NBR IN (0,1,365)
                AND DT.TRNFM_NBR IN (4,13,26,52)
                AND DT.CAL_YR_WK_STOP_DT = WEEK_ENDING
				group by 1,2;
				
/*						select trnfm_last_yr_nbr,TRNFM_NBR, 
			   to_char(TO_DATE(min(date_key),'YYYYMMDD')- interval '6 days','yyyy-mm-dd') AS START_DATE,
				to_char(TO_DATE(max(date_key),'YYYYMMDD') ,'yyyy-mm-dd') AS END_DATE 
				from
                DATE_CALENDAR_YEAR_WK2DY_RLU_V A16            
                JOIN      DATE_TRNFM_CALENDAR_YR_WK_V DT          ON (DT.TRNFM_CAL_YR_WK_STOP_DT = A16.CAL_YR_WK_STOP_DT)
                WHERE A16.CAL_DOW_NBR = 6
                AND DT.CAL_DOW_NBR = 6
                AND DT.TRNFM_LAST_YR_NBR IN (0)
                AND DT.TRNFM_NBR IN (4)
                AND DT.CAL_YR_WK_STOP_DT = '2019-09-21'
				group by 1,2;	
	*/			
RAISE NOTICE 'NUM_RTL_HUB_DATE  TABLE CREATED FOR WEEK-4,13,26,52';


 
 INSERT INTO NUM_RTL_HUB_DATE 
 select trnfm_last_yr_nbr,TRNFM_NBR,to_char(date(trnfm_cal_yr_wk_stop_dt) - interval '6 days','yyyy-mm-dd') AS START_DATE,TO_DATE(date_key,'YYYYMMDD') AS END_DATE from 
 DATE_CALENDAR_YEAR_WK2DY_RLU_V A16            
 JOIN      DATE_TRNFM_CALENDAR_YR_WK_V DT          ON (DT.TRNFM_CAL_YR_WK_STOP_DT = A16.CAL_YR_WK_STOP_DT)
 WHERE A16.CAL_DOW_NBR = DOW_NBR
 AND DT.CAL_DOW_NBR = DOW_NBR
 AND DT.TRNFM_LAST_YR_NBR IN (0,1,365)
 AND DT.TRNFM_NBR IN (1)
 AND DT.CAL_YR_WK_STOP_DT = WEEK_ENDING;
 
 RAISE NOTICE 'INSERT  FOR  WEEK-1 INTO TABLE NUM_RTL_HUB_DATE  is COMPLETED ';
 
 TRUNCATE TABLE PN1USSA1..NUM_RTL_HUB_CHN_SOW_02;
 TRUNCATE TABLE PN1USSA1..NUM_RTL_HUB_SUBCAT_SOW_03;
 TRUNCATE TABLE PN1USSA1..ks_shoprite_sales ;
 

 call  mi_hub360_rtl_num_SP(0,4);
  call  mi_hub360_rtl_num_SP(0,13);
--call  mi_hub360_rtl_num_SP(0,1); 
--call mi_hub360_rtl_num_SP(1,4); 
 --call mi_hub360_rtl_num_SP(1,1);
 --call mi_hub360_rtl_num_SP(1,13);
 --call  mi_hub360_rtl_num_SP(0,26);
 --call mi_hub360_rtl_num_SP(1,26);
-- call  mi_hub360_rtl_num_SP(0,52);
-- call mi_hub360_rtl_num_SP(1,52);
 
 RETURN 0; 
 END; 
 END_PROC;