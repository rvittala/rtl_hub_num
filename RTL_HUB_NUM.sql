--Tab 1: Spend By channel and banner_id (for core - people who make up the top 80% of sales)
-- limit to Grocery to get the competing retailers
drop table PN1USSA1.NUM_RTL_HUB_CHN_SOW_00 if exists;
create table PN1USSA1.NUM_RTL_HUB_CHN_SOW_00  as 
select x.*, NPY, nvl(py_retailer_sector_spend, 0.0) as py_retailer_spend , nvl(py_percent_spend,0.0) as py_percent_spend,
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend as varchar(255))
     else 'undefined' end as pct_change_cy_vs_py
FROM 
        (select a.*, cy_retailer_spend / sector_spend as cy_percent_spend
        from 
                (select channel, x.banner_id, COUNT(*) AS NCY, sum(nvl(sum_amount,0.0)) as cy_retailer_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id and sector_description='Grocery'
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend                               from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > '2018-09-04 00:00:00'
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > '2018-09-04 00:00:00'
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE transaction_date > '2018-09-04 00:00:00'     
                group by channel, x.banner_id
                ) as a
        cross join
                (select sum(nvl(sum_amount,0.0)) as sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id and sector_description='Grocery'
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > '2018-09-04 00:00:00'
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > '2018-09-04 00:00:00'
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date > '2018-09-04 00:00:00'                       
                ) as b
        ) as x
LEFT JOIN  
        (select a.*, py_retailer_sector_spend / py_sector_spend as py_percent_spend
        from 
                (select channel, x.banner_id, COUNT(*) AS NPY, sum(nvl(sum_amount,0.0)) as py_retailer_sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id and sector_description='Grocery'
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date <= '2018-09-04 00:00:00'                       
                                                AND transaction_date > '2017-09-04 00:00:00'                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date <= '2018-09-04 00:00:00'                       
                                        AND transaction_date > '2017-09-04 00:00:00'                      
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE  transaction_date <= '2018-09-04 00:00:00'                       
                AND transaction_date > '2017-09-04 00:00:00'                       
                group by channel, x.banner_id
                ) as a
        cross join
                (select sum(nvl(sum_amount,0.0)) as py_sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id and sector_description='Grocery'
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date <= '2018-09-04 00:00:00'                       
                                                AND transaction_date > '2017-09-04 00:00:00'                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date <= '2018-09-04 00:00:00'                       
                                        AND transaction_date > '2017-09-04 00:00:00'                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date <= '2018-09-04 00:00:00'                       
                AND transaction_date > '2017-09-04 00:00:00'                      
                ) as b
        ) as y
                ON (x.channel = y.channel
                and x.banner_id = y.banner_id)
;


-- TO GET TO THE TOP 4 MAKJOR COMPETITORS OF TEH RETAILER, the retialer and the rest (Others)
drop table  RTL_HUB_CHN_EXCEPT_RETAILER if exists;
create temporary table RTL_HUB_CHN_EXCEPT_RETAILER as
SELECT channel,banner_id,ncy,cy_retailer_spend,cy_percent_spend,npy,py_retailer_spend,py_percent_spend FROM 
(SELECT *, ROW_NUMBER() OVER (ORDER BY cy_percent_spend DESC) AS rank FROM PN1USSA1.NUM_RTL_HUB_CHN_SOW_00 where banner_id not in ('shop_rite')) tmp 
WHERE rank <= 4
;


drop table  RTL_HUB_CHN_OTHERS if exists;
create temporary table RTL_HUB_CHN_OTHERS as 
SELECT 'MIXED' as channel, 'OTHERS' as banner_id,
sum(ncy) as ncy, sum(cy_retailer_spend) as cy_retailer_spend, sum(cy_percent_spend) as cy_percent_spend, 
sum(npy) as npy, sum(py_retailer_spend) as py_retailer_spend, sum(py_percent_spend) as py_percent_spend
FROM PN1USSA1.NUM_RTL_HUB_CHN_SOW_00 a
left join (select distinct channel, banner_id from RTL_HUB_CHN_EXCEPT_RETAILER) b on (a.channel=b.channel and a.banner_id=b.banner_id)
where a.banner_id not in ('shop_rite') and b.banner_id is null
;

drop table  RTL_HUB_BANNERS6  if exists;
create table RTL_HUB_BANNERS6 as
select * from RTL_HUB_CHN_EXCEPT_RETAILER a
union all 
select * from RTL_HUB_CHN_OTHERS b
union all
select channel,banner_id,ncy,cy_retailer_spend,cy_percent_spend,npy,py_retailer_spend,py_percent_spend from PN1USSA1.NUM_RTL_HUB_CHN_SOW_00 where banner_id in ('shop_rite')
;


drop table NUM_RTL_HUB_CHN_SOW_02 if exists;
create table NUM_RTL_HUB_CHN_SOW_02 AS
-- Data for Visual 1
select channel,	banner_id, ncy,	cy_retailer_spend, cy_percent_spend, npy, 
--py_retailer_spend 
 py_percent_spend, 
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend as varchar(255)) 
else 'undefined' end as pct_change_cy_vs_py,
ROW_NUMBER() OVER (ORDER BY cy_percent_spend DESC) AS rank
from RTL_HUB_BANNERS6 order by cy_percent_spend desc;




--Tab 2: Spend By sector, channel and banner_id (for core - people who make up the top 80% of sales)
-- concatenation of cells to represent 6 groups will be done at the end
drop table  pn1ussa1.NUM_RTL_HUB_SEC_SOW_00 if exists ;
create table pn1ussa1.NUM_RTL_HUB_SEC_SOW_00  as 
select x.*, NPY, nvl(py_retailer_sector_spend, 0.0) as py_retailer_sector_spend , nvl(py_percent_spend,0.0) as py_percent_spend
FROM 
        (select a.*, cy_retailer_sector_spend / NULLIF(sector_spend,0) as cy_percent_spend
        from 
                (select sector_description, channel, x.banner_id, COUNT(*) AS NCY, sum(nvl(sum_amount,0.0)) as cy_retailer_sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / NULLIF(total_spend,0) as percent_spend                               from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > '2018-09-04 00:00:00'
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > '2018-09-04 00:00:00'
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE transaction_date > '2018-09-04 00:00:00'     
                group by sector_description, channel, x.banner_id
                ) as a
        JOIN
                (select sector_description, sum(nvl(sum_amount,0.0)) as sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum / NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > '2018-09-04 00:00:00'
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > '2018-09-04 00:00:00'
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date > '2018-09-04 00:00:00'                       
                group by sector_description
                ) as b
                        ON a.sector_description = b.sector_description
                        AND a.sector_description != ''
        ) as x
LEFT JOIN  
        (select a.*, py_retailer_sector_spend / py_sector_spend as py_percent_spend
        from 
                (select sector_description, channel, x.banner_id, COUNT(*) AS NPY, sum(nvl(sum_amount,0.0)) as py_retailer_sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND  transaction_date <= '2018-09-04 00:00:00'                       
                                                AND transaction_date > '2017-09-04 00:00:00'                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND  transaction_date <= '2018-09-04 00:00:00'                       
                                        AND transaction_date > '2017-09-04 00:00:00'                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE  transaction_date <= '2018-09-04 00:00:00'                       
                AND transaction_date > '2017-09-04 00:00:00'                       
                group by sector_description, channel, x.banner_id
                ) as a
        JOIN
                (
                
                select sector_description, sum(nvl(sum_amount,0.0)) as py_sector_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN 
                        (
                        
                        select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum / total_spend as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND  transaction_date <= '2018-09-04 00:00:00'                       
                                                AND transaction_date > '2017-09-04 00:00:00'                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date <= '2018-09-04 00:00:00'                       
                                        AND transaction_date > '2017-09-04 00:00:00'                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 
                        
                        ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date <= '2018-09-04 00:00:00'                       
                AND transaction_date > '2017-09-04 00:00:00'                       
                group by sector_description
                
                
                ) as b
                        ON a.sector_description = b.sector_description
                        AND a.sector_description != ''
        ) as y
                ON x.sector_description = y.sector_description
                AND x.channel = y.channel
                and x.banner_id = y.banner_id
WHERE x.sector_description != 'unknown'                
order by x.sector_description
;


-- Reduce to banners6 chosen at Overall spend
drop table  NUM_RTL_HUB_SEC_OTHERS if exists;
create temporary table NUM_RTL_HUB_SEC_OTHERS as
select sector_description, 
'MIXED' as channel, 'OTHERS' as banner_id,
sum(ncy) as ncy, sum(cy_retailer_sector_spend) as cy_retailer_sector_spend, sum(cy_percent_spend) as cy_percent_spend, 
sum(npy) as npy, sum(py_retailer_sector_spend) as py_retailer_sector_spend, sum(py_percent_spend) as py_percent_spend
from 
pn1ussa1.NUM_RTL_HUB_SEC_SOW_00  a
left join (select distinct banner_id from RTL_HUB_BANNERS6 where banner_id <> 'OTHERS') b on a.banner_id=b.banner_id
where b.banner_id is null
group by 1,2,3;

drop table NUM_RTL_HUB_SEC_SOW_MAIN if exists;
create temporary table NUM_RTL_HUB_SEC_SOW_MAIN as
select a.*
from 
pn1ussa1.NUM_RTL_HUB_SEC_SOW_00 a
left join (select distinct banner_id from RTL_HUB_BANNERS6 where banner_id <> 'OTHERS') b on a.banner_id=b.banner_id
where b.banner_id is not null;

drop table  NUM_RTL_HUB_SEC_SOW_01 if exists;
create  temporary table NUM_RTL_HUB_SEC_SOW_01 as
select * from NUM_RTL_HUB_SEC_SOW_MAIN a
union all 
select * from NUM_RTL_HUB_SEC_OTHERS;

drop table NUM_RTL_HUB_SEC_SOW_02 if exists;
create table NUM_RTL_HUB_SEC_SOW_02 AS
SELECT sector_description,	channel	banner_id,	ncy,	cy_retailer_sector_spend,	cy_percent_spend,	npy,	/* py_retailer_sector_spend */	py_percent_spend, 
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend  AS varchar(255))
else 'undefined' end as pct_change_cy_vs_py,
ROW_NUMBER() OVER (PARTITION BY sector_description ORDER BY cy_retailer_sector_spend DESC) AS rank FROM 
(
select a.* from 
NUM_RTL_HUB_SEC_SOW_01 a
join
(select distinct sector_description from NUM_RTL_HUB_SEC_SOW_01 where banner_id='shop_rite' and cy_percent_spend > 0.00) b 
on (a.sector_description=b.sector_description)
) c
order by sector_description, rank;



--Tab 3: Spend By sector, department, channel and banner_id (for core - people who make up the top 80% of sales)
-- concatenation of cells to represent 6 groups will be done at the end
drop table pn1ussa1.NUM_RTL_HUB_DEPT_SOW_00 if exists ;
create table pn1ussa1.NUM_RTL_HUB_DEPT_SOW_00  as 
select x.*, NPY, nvl(py_retailer_department_spend, 0.0) as py_retailer_department_spend , nvl(py_percent_spend,0.0) as py_percent_spend
FROM 
        (select a.*, cy_retailer_department_spend / NULLIF(department_spend,0) as cy_percent_spend
        from 
                (select sector_description, department_description, channel, x.banner_id, COUNT(*) AS NCY, sum(nvl(sum_amount,0.0)) as cy_retailer_department_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / NULLIF(total_spend,0) as percent_spend                               from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > '2018-09-04 00:00:00'
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > '2018-09-04 00:00:00'
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE transaction_date > '2018-09-04 00:00:00'     
                group by sector_description, department_description, channel, x.banner_id
                ) as a
        JOIN
                (select department_description, sum(nvl(sum_amount,0.0)) as department_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum / NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > '2018-09-04 00:00:00'
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > '2018-09-04 00:00:00'
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date > '2018-09-04 00:00:00'                       
                group by department_description
                ) as b
                        ON a.department_description = b.department_description
                        AND a.department_description != ''
        ) as x
LEFT JOIN  
        (select a.*, py_retailer_department_spend / NULLIF(py_department_spend,0) as py_percent_spend
        from 
                (select department_description, channel, x.banner_id, COUNT(*) AS NPY, sum(nvl(sum_amount,0.0)) as py_retailer_department_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND  transaction_date <= '2018-09-04 00:00:00'                       
                                                AND transaction_date > '2017-09-04 00:00:00'                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND  transaction_date <= '2018-09-04 00:00:00'                       
                                        AND transaction_date > '2017-09-04 00:00:00'                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE  transaction_date <= '2018-09-04 00:00:00'                       
                AND transaction_date > '2017-09-04 00:00:00'                       
                group by department_description, channel, x.banner_id
                ) as a
        JOIN
                (select department_description, sum(nvl(sum_amount,0.0)) as py_department_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum / NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND  transaction_date <= '2018-09-04 00:00:00'                       
                                                AND transaction_date > '2017-09-04 00:00:00'                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date <= '2018-09-04 00:00:00'                       
                                        AND transaction_date > '2017-09-04 00:00:00'                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date <= '2018-09-04 00:00:00'                       
                AND transaction_date > '2017-09-04 00:00:00'                       
                group by department_description
                ) as b
                        ON a.department_description = b.department_description
                        AND a.department_description != ''
        ) as y
                ON x.department_description = y.department_description
                AND x.channel = y.channel
                and x.banner_id = y.banner_id
WHERE x.department_description != 'unknown'                
order by x.department_description
;

-- Reduce to banners6 chosen at Overall spend
drop table  NUM_RTL_HUB_DEPT_SOW_OTHERS if exists;
create  temporary table NUM_RTL_HUB_DEPT_SOW_OTHERS as
select sector_description, department_description,
'MIXED' as channel, 'OTHERS' as banner_id,
sum(ncy) as ncy, sum(cy_retailer_department_spend) as cy_retailer_department_spend, sum(cy_percent_spend) as cy_percent_spend, 
sum(npy) as npy, sum(py_retailer_department_spend) as py_retailer_department_spend, sum(py_percent_spend) as py_percent_spend
from 
PN1USSA1.NUM_RTL_HUB_DEPT_SOW_00 a
left join (select distinct banner_id from RTL_HUB_BANNERS6 where banner_id <> 'OTHERS') b on a.banner_id=b.banner_id
where b.banner_id is null
group by 1,2,3,4;
--  records
drop table NUM_RTL_HUB_DEPT_SOW_MAIN  if exists;
create temporary table NUM_RTL_HUB_DEPT_SOW_MAIN as
select a.*
from 
PN1USSA1.NUM_RTL_HUB_DEPT_SOW_00 a
left join (select distinct banner_id from RTL_HUB_BANNERS6 where banner_id <> 'OTHERS') b on a.banner_id=b.banner_id
where b.banner_id is not null;
--  records

drop table NUM_RTL_HUB_DEPT_SOW_01 if exists;
create  temporary table NUM_RTL_HUB_DEPT_SOW_01 as
select * from NUM_RTL_HUB_DEPT_SOW_MAIN a
union all 
select * from NUM_RTL_HUB_DEPT_SOW_OTHERS;


drop table NUM_RTL_HUB_DEPT_SOW_02 if exists;
create table NUM_RTL_HUB_DEPT_SOW_02 AS
SELECT sector_description,	department_description,	channel	banner_id,	ncy,	cy_retailer_department_spend,	cy_percent_spend,	npy,	/* py_retailer_department_spend	*/ py_percent_spend, 
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend as varchar(255))
else 'undefined' end as pct_change_cy_vs_py,
ROW_NUMBER() OVER (PARTITION BY department_description ORDER BY cy_retailer_department_spend DESC) AS rank FROM 
(
select a.* from 
NUM_RTL_HUB_DEPT_SOW_01 a
join
(select distinct department_description from NUM_RTL_HUB_DEPT_SOW_01 where banner_id='shop_rite' and cy_percent_spend > 0.00) b 
on (a.department_description=b.department_description)
) c
order by sector_description, department_description, rank;



--Tab 4: Spend By sector, department, category, channel and banner_id (for core - people who make up the top 80% of sales)
-- concatenation of cells to represent 6 groups will be done later

-- make this a temp table?
drop table  PN1USSA1.NUM_RTL_HUB_CAT_SOW_00 if exists;
create table PN1USSA1.NUM_RTL_HUB_CAT_SOW_00  as 
select x.*, NPY, nvl(py_retailer_category_spend, 0.0) as py_retailer_category_spend , nvl(py_percent_spend,0.0) as py_percent_spend
FROM 
        (select a.*, cy_retailer_category_spend /  NULLIF(category_spend,0) as cy_percent_spend
        from 
                (select sector_description, department_description, category_description,  channel, x.banner_id, COUNT(*) AS NCY, sum(nvl(sum_amount,0.0)) as cy_retailer_category_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum / NULLIF(total_spend,0) as percent_spend                               from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > '2018-09-04 00:00:00'
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > '2018-09-04 00:00:00'
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE transaction_date > '2018-09-04 00:00:00'     
                group by sector_description, department_description, category_description,  channel, x.banner_id
                ) as a
        JOIN
                (select department_description, category_description,  sum(nvl(sum_amount,0.0)) as category_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum /  NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND transaction_date > '2018-09-04 00:00:00'
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date > '2018-09-04 00:00:00'
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date > '2018-09-04 00:00:00'                       
                group by department_description, category_description
                ) as b
                        ON a.department_description = b.department_description
                        and a.category_description = b.category_description
                        AND a.category_description != ''
                        AND a.department_description != ''
        ) as x
LEFT JOIN  
        (select a.*, py_retailer_category_spend /  NULLIF(py_category_spend,0) as py_percent_spend
        from 
                (select department_description, category_description,  channel, x.banner_id, COUNT(*) AS NPY, sum(nvl(sum_amount,0.0)) as py_retailer_category_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN
                  (select distinct user_id
                  from 
                            (
                            select   a.*, spending_cum_sum /  NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND  transaction_date <= '2018-09-04 00:00:00'                       
                                                AND transaction_date > '2017-09-04 00:00:00'                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND  transaction_date <= '2018-09-04 00:00:00'                       
                                        AND transaction_date > '2017-09-04 00:00:00'                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id
                WHERE  transaction_date <= '2018-09-04 00:00:00'                       
                AND transaction_date > '2017-09-04 00:00:00'                       
                group by department_description, category_description,  channel, x.banner_id
                ) as a
        JOIN
                (select department_description, category_description,  sum(nvl(sum_amount,0.0)) as py_category_spend
                FROM pn1ussa1..numerator_fact_v x
                JOIN pn1ussa1..numerator_banner_v b
                                on x.banner_id = b.banner_id
                                AND channel not in ('Pet', 'Liquor', 'Military', 'Online')
                JOIN pn1ussa1..numerator_item_v  i
                                ON x.item_id = i.item_id
                JOIN 
                        (select distinct user_id
                        from 
                            (
                            select   a.*, spending_cum_sum /  NULLIF(total_spend,0) as percent_spend
                                from 
                                        (SELECT user_id, wakefern_spend, sum(wakefern_spend) OVER (ORDER BY wakefern_spend desc rows unbounded preceding) as spending_cum_sum
                                        FROM 
                                                (select x.user_id,  sum(nvl(sum_amount,0.0)) as wakefern_spend
                                                FROM pn1ussa1..numerator_fact_v x
                                                WHERE banner_id in ('shop_rite')  
                                                AND  transaction_date <= '2018-09-04 00:00:00'                       
                                                AND transaction_date > '2017-09-04 00:00:00'                       
                                                group by x.user_id) as i
                                         ) as a            
                                cross join 
                                        (select  sum(nvl(sum_amount,0.0)) as total_spend
                                        FROM pn1ussa1..numerator_fact_v x
                                        WHERE banner_id in ('shop_rite')  
                                        AND transaction_date <= '2018-09-04 00:00:00'                       
                                        AND transaction_date > '2017-09-04 00:00:00'                       
                                        ) as b       
                                ) as x
                        WHERE percent_spend <=  0.8 ) as a            
                        on x.user_id = a.user_id 
                WHERE transaction_date <= '2018-09-04 00:00:00'                       
                AND transaction_date > '2017-09-04 00:00:00'                       
                group by department_description, category_description
                ) as b
                        ON a.department_description = b.department_description
                        and a.category_description = b.category_description
                        AND a.category_description != ''
                        AND a.department_description != ''
        ) as y
                ON x.department_description = y.department_description
                AND x.category_description = y.category_description
                AND x.channel = y.channel
                and x.banner_id = y.banner_id
WHERE x.department_description != 'unknown'                
order by x.department_description, x.category_description
;

-- Reduce to banners6 chosen at Overall spend
drop table NUM_RTL_HUB_CAT_SOW_OTHERS if exists ;
create  temporary table NUM_RTL_HUB_CAT_SOW_OTHERS as
select sector_description, department_description, category_description,
'MIXED' as channel, 'OTHERS' as banner_id,
sum(ncy) as ncy, sum(cy_retailer_category_spend) as cy_retailer_category_spend, sum(cy_percent_spend) as cy_percent_spend, 
sum(npy) as npy, sum(py_retailer_category_spend) as py_retailer_category_spend, sum(py_percent_spend) as py_percent_spend
from 
PN1USSA1.NUM_RTL_HUB_CAT_SOW_00 a
left join (select distinct banner_id from RTL_HUB_BANNERS6 where banner_id <> 'OTHERS') b on a.banner_id=b.banner_id
where b.banner_id is null
group by 1,2,3,4,5;
-- 1823 records
drop table  NUM_RTL_HUB_CAT_SOW_MAIN if exists;
create temporary table NUM_RTL_HUB_CAT_SOW_MAIN as
select a.*
from 
PN1USSA1.NUM_RTL_HUB_CAT_SOW_00 a
left join (select distinct banner_id from RTL_HUB_BANNERS6 where banner_id <> 'OTHERS') b on a.banner_id=b.banner_id
where b.banner_id is not null;
-- 5647 records

drop table  NUM_RTL_HUB_CAT_SOW_01 if exists; 
create temporary table NUM_RTL_HUB_CAT_SOW_01 as
select * from NUM_RTL_HUB_CAT_SOW_MAIN a
union all 
select * from NUM_RTL_HUB_CAT_SOW_OTHERS;



drop table NUM_RTL_HUB_CAT_SOW_02 if exists;
create table NUM_RTL_HUB_CAT_SOW_02 as 
SELECT sector_description, department_description, category_description, channel, banner_id, ncy, cy_retailer_category_spend, cy_percent_spend, npy, /* py_retailer_category_spend */ py_percent_spend, 
case when py_percent_spend > 0 then cast((cy_percent_spend-py_percent_spend)/py_percent_spend AS VARCHAR(255))
else 'undefined' end as pct_change_cy_vs_py,
ROW_NUMBER() OVER (PARTITION BY department_description, category_description ORDER BY cy_retailer_category_spend DESC) AS rank FROM 
(
select a.* from 
NUM_RTL_HUB_CAT_SOW_01 a
join
(select distinct department_description, category_description from NUM_RTL_HUB_CAT_SOW_01 where banner_id='shop_rite' and cy_percent_spend > 0.00) b 
on (a.department_description=b.department_description and a.category_description=b.category_description)
) c
order by sector_description, department_description, category_description, rank;





--FINAL DATASET

drop table NUM_RTL_HUB_SOW_MAIN_DATASET_T if exists;
CREATE TABLE NUM_RTL_HUB_SOW_MAIN_DATASET_T AS
SELECT * FROM NUM_RTL_HUB_CAT_SOW_02;

INSERT INTO NUM_RTL_HUB_SOW_MAIN_DATASET_T
SELECT * FROM NUM_RTL_HUB_DEPT_SOW_02;

INSERT INTO NUM_RTL_HUB_SOW_MAIN_DATASET_T
SELECT * FROM NUM_RTL_HUB_SEC_SOW_02;

INSERT INTO NUM_RTL_HUB_SOW_MAIN_DATASET_T
SELECT * FROM NUM_RTL_HUB_CHN_SOW_02;


